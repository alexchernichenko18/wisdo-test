import useMediaQuery from './useMedia'

const BREAKPOINT_SM = '767px'
const BREAKPOINT_MD = '990px'

export default function useBreakpoints() {
  const isTablet = useMediaQuery(`(min-width: ${+BREAKPOINT_SM.split('px')[0] + 1}px) and (max-width: ${BREAKPOINT_MD})`)
  const isDesktop = useMediaQuery(`(min-width: ${+BREAKPOINT_MD.split('px')[0] + 1}px)`)
  const isLargeDesktop = useMediaQuery('(min-width: 1280px)')
  const isMobile = !isTablet && !isDesktop

  return {
    isMobile,
    isTablet,
    isDesktop,
    isLargeDesktop,
  }
}
