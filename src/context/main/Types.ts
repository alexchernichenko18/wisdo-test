export interface IItem {
  lifeChallengeId: string
  title: string
  description: string
  image: string
  membersCount: number
  isFavorite: boolean
}

export interface ICategory {
  categoryName: string
  items: IItem[]
  id: string
}

export interface IIdsParams {
  categoryId: string
  itemId: string
}
export interface State {
  categories: ICategory[]
  isFavoritesMode: boolean
}

export enum ACTIONS {
  SET_CATEGORIES = 'set_categories',
  SET_ITEM_FAVORITE_FLAG = 'set_item_favorite_flag',
  SET_IS_FAVORITES_MODE = 'set_is_favorites_mode',
}

interface SET_CATEGORIES {
  type: ACTIONS.SET_CATEGORIES
  payload: ICategory[]
}

interface SET_ITEM_FAVORITE_FLAG {
  type: ACTIONS.SET_ITEM_FAVORITE_FLAG
  payload: IIdsParams
}

interface SET_IS_FAVORITES_MODE {
  type: ACTIONS.SET_IS_FAVORITES_MODE
  payload: boolean
}

export type Reducers = SET_CATEGORIES | SET_ITEM_FAVORITE_FLAG | SET_IS_FAVORITES_MODE
