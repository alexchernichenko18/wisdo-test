import { AppContext } from './AppContext'
import useAppController from './useAppController'

export const AppProvider = ({ children }: any) => {
  const contextValue = useAppController()

  return <AppContext.Provider value={contextValue}>{children}</AppContext.Provider>
}
