import { createContext, useContext } from 'react'
import useAppController from './useAppController'

export const AppContext = createContext({} as ReturnType<typeof useAppController>)

export const useAppContext = () => useContext(AppContext)
