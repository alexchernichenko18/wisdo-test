import { useReducer } from 'react'
import { initialState, reducer } from './State'
import { ACTIONS, ICategory, IIdsParams } from './Types'

const useAppController = () => {
  const [state, dispatch] = useReducer(reducer, initialState)
  const {
    categories,
    isFavoritesMode,
  } = state

  const setCategories = (newCategories: ICategory[]): void => {
    dispatch({
      type: ACTIONS.SET_CATEGORIES,
      payload: newCategories,
    })
  }

  const setItemFavoriteFlag = ({ categoryId, itemId }: IIdsParams): void => {
    dispatch({
      type: ACTIONS.SET_ITEM_FAVORITE_FLAG,
      payload: { categoryId, itemId },
    })
  }

  const setIsFavoritesMode = (newFlag: boolean): void => {
    dispatch({
      type: ACTIONS.SET_IS_FAVORITES_MODE,
      payload: newFlag,
    })
  }

  return {
    categories,
    isFavoritesMode,
    setCategories,
    setItemFavoriteFlag,
    setIsFavoritesMode
  }
}

export default useAppController
