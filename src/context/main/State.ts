import { ACTIONS, State, Reducers } from './Types'

export const initialState: State = {
  categories: [],
  isFavoritesMode: false,
}

export const reducer = (state: State, action: Reducers): State => {
  switch (action.type) {
    case ACTIONS.SET_CATEGORIES: {
      return {
        ...state,
        categories: action.payload,
      }
    }
    case ACTIONS.SET_ITEM_FAVORITE_FLAG: {
      const { categoryId, itemId } = action.payload

      const newCategories = state.categories.map(category => {
        if (category.id === categoryId) {
          return {
            ...category,
            items: category.items.map((item => {
              if (item.lifeChallengeId === itemId) {
                return {
                  ...item,
                  isFavorite: !item.isFavorite,
                }
              }

              return item
            }))
          }
        }

        return category
      })

      return {
        ...state,
        categories: newCategories,
      }
    }
    case ACTIONS.SET_IS_FAVORITES_MODE: {
      return {
        ...state,
        isFavoritesMode: action.payload,
      }
    }
    default: {
      return state
    }
  }
}
