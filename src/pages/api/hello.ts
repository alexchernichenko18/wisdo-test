import { NextApiRequest, NextApiResponse } from 'next'

export default function hello(_req: NextApiRequest, res: NextApiResponse): void {
  res.status(200).json({ name: 'John Doe' })
}
