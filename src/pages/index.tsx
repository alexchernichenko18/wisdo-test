import Main from 'components/Main/Main'
import { ICategory, useAppContext } from 'context'
import { useEffect } from 'react'

export default function Home({ data }: any) {
  const { setCategories } = useAppContext()

  useEffect(() => {
    const parsedData = data.map((category: ICategory, index: number) => {
      return {
        ...category,
        id: `${index}`,
        items: category.items.map((item) => ({ ...item, isFavorite: false }))
      }
    })
    setCategories(parsedData)
  }, [])

  return (
    <Main />
  )
}

export const getServerSideProps = async () => {
  const response = await fetch('https://wisdo-test-react.herokuapp.com/feed')
  const data = await response.json()

  return {
    props: {
      data,
    }
  }
}
