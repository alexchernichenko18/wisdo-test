import Header from 'components/Header/Header'
import Footer from 'components/Footer/Footer'
import { AppProvider } from 'context'

import '../styles/globals.scss'

function MyApp({ Component, pageProps }: any) {
  return (
    <AppProvider>
      <Header />
      <Component {...pageProps} />
      <Footer />
    </AppProvider>
  )
}

export default MyApp
