import Button from 'components/Button/Button'
import styles from './Footer.module.scss'

export default function Footer () {
  return (
    <div className={styles.footer}>
      <div className={styles.container}>
        <div className={styles.info}>
          <div className={styles.title}>Join the conversation!</div>
          <div className={styles.subtitle}>Sign in to talk to real people, whom have been there and can help.</div>
        </div>
        <Button>Sign up</Button>
      </div>
    </div>
  )
}
