import { useMemo, useState } from 'react'
import Slider from 'react-slick'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'
import { FavoriteIcon, FullFavoriteIcon, ArrowIcon } from 'assets/icons'
import { ICategory, IItem, useAppContext } from 'context'
import cn from 'classnames'
import styles from './Category.module.scss'
import useBreakpoints from 'hooks/useBreakpoints'

const getMembersCount = (count: number) => {
  return Math.trunc(count / 1000)
}

const SamplePrevArrow = ({ className, onClick }: any) => {
  return (
    <div
      className={cn(className, styles.prevArrow)}
      onClick={onClick}
    >
      <ArrowIcon />
    </div>
  )
}

const SampleNextArrow = ({ className, onClick }: any) => {
  return (
    <div
      className={cn(className, styles.nextArrow)}
      onClick={onClick}
    >
      <ArrowIcon />
    </div>
  )
}

export default function Category ({
  categoryName,
  items,
  id,
}: ICategory) {
  const { isFavoritesMode, setItemFavoriteFlag } = useAppContext()
  const [renderedItemsCount, setRenderedItemsCount] = useState<number>(0)
  const { isMobile } = useBreakpoints()
  const isSliderMode = renderedItemsCount > (isMobile ? 2 : 4)

  const settings = useMemo(() => {
    return {
      nextArrow: <SampleNextArrow />,
      prevArrow: <SamplePrevArrow />,
      className: 'slider variable-width',
      dots: false,
      infinite: false,
      centerMode: false,
      slidesToShow: isMobile ? 2 : 4,
      slidesToScroll: 1,
      variableWidth: true,
    }
  }, [isMobile])

  const setItemFavoriteFlagHandler = (id: string, lifeChallengeId: string, isFavorite: boolean) => {
    if (isFavorite) {
      if (window.confirm("Are you sure?")) {
        setItemFavoriteFlag({ categoryId: id, itemId: lifeChallengeId })
        return
      } else {
        return
      }
    }

    setItemFavoriteFlag({ categoryId: id, itemId: lifeChallengeId })
  }

  const itemsRender = useMemo(() => {
    let newRenderedItems: any = isFavoritesMode ? items.filter(item => item.isFavorite) : items
    newRenderedItems = newRenderedItems.map((item: IItem) => {
      const { title, membersCount, image, lifeChallengeId, isFavorite } = item
      return (
        <div key={lifeChallengeId} className={styles.imageItem}>
          <div className={styles.imageWrap}>
            <img src={image} alt="" />
            <div className={styles.favoriteWrap} onClick={() => setItemFavoriteFlagHandler(id, lifeChallengeId, isFavorite)}>
              {isFavorite ? <FullFavoriteIcon /> : <FavoriteIcon />}
            </div>
          </div>
          <div className={styles.info}>
            <div className={styles.infoTitle}>{title}</div>
            <div className={styles.infoSubtitle}>${getMembersCount(membersCount)}K Members</div>
          </div>
        </div>
      )
    })

    setRenderedItemsCount(newRenderedItems.length)

    return newRenderedItems
  }, [items, isFavoritesMode])

  return (
    <div className={styles.categoryItemsWrap}>
      <div className={styles.categoryTitle}>{categoryName}</div>
      <div className={styles.categorySubtitle}>Identity</div>
      <div className={cn(styles.imagesWrap, !isSliderMode && styles.noSliderMode)}>
        {isSliderMode ? (
          <div className={styles.carousel}>
            <Slider {...settings}>
              {itemsRender}
            </Slider>
          </div>
        ) : (
          itemsRender
        )}
      </div>
    </div>
  )
}
