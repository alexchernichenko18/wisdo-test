import { LogoIcon, ExploreIcon } from 'assets/icons'
import Button from 'components/Button/Button'
import { OUTLINE_TYPE, LIGHT_TYPE } from 'constants/main'

import styles from './Header.module.scss'

export default function Header () {
  return (
    <div className={styles.header}>
      <div className={styles.container}>
        <div className={styles.innerFlexWrap}>
          <div className={styles.leftColumnWrap}>
            <div className={styles.logo}>
              <LogoIcon />
            </div>
          </div>
          <div className={styles.centerColumnWrap}>
            <div className={styles.title}>Communities</div>
          </div>
        </div>
        <div className={styles.rightColumnWrap}>
          <div className={styles.buttonsWrap}>
            <Button type={LIGHT_TYPE} icon={<ExploreIcon />}>Explore communities</Button>
            <Button type={OUTLINE_TYPE} icon={<ExploreIcon />}>Log in</Button>
            <Button icon={<ExploreIcon />}>Sign up</Button>
          </div>
        </div>
      </div>
    </div>
  )
}
