import cn from 'classnames'
import { DEFAULT_TYPE } from 'constants/main'
import styles from './Button.module.scss'

export default function Button({ children, icon = null, type = DEFAULT_TYPE }: any) {
  return (
    <div
      className={
        cn(
          styles.button,
          !!icon && styles.withIcon,
          styles[`${type}Type`],
        )
      }
    >
      {icon && (
        <div className={styles.icon}>{icon}</div>
      )}
      <span>{children}</span>
    </div>
  )
}
