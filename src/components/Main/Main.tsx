import { CommunitiesIcon, StarIcon } from 'assets/icons'
import Category from 'components/Category/Category'
import cn from 'classnames'
import styles from './Main.module.scss'
import { useAppContext } from 'context'

export default function Main () {
  const { categories, isFavoritesMode, setIsFavoritesMode } = useAppContext()
  
  return (
    <div className={styles.main}>
      <div className={styles.sidebar}>
        <div className={styles.categories}>
          <div className={cn(styles.category, !isFavoritesMode && styles.active)} onClick={() => setIsFavoritesMode(false)}>
            <div className={styles.icon}>
              <CommunitiesIcon />
            </div>
            <span>Communities</span>
          </div>
          <div className={cn(styles.category, isFavoritesMode && styles.active)} onClick={() => setIsFavoritesMode(true)}>
            <div className={styles.icon}>
              <StarIcon />
            </div>
            <span>Favorites</span>
          </div>
        </div>
      </div>
      <div className={styles.content}>
        <div className={styles.inner}>
          {categories?.length ? categories.map((category) => {
            const isRender = !isFavoritesMode || category.items.some(item => item.isFavorite)
            if (!isRender) {
              return null
            }

            return (
              <div key={category.id}>
                <Category {...category} />
              </div>
            )
          }) : null}
        </div>
      </div>
    </div>
  )
}
